
# If use attached disk
resource "google_compute_instance" "instance_with_attached_disk" {
  count        = var.attached_disk_enabled ? 1 : 0
  name         = var.instance_name
  machine_type = var.machine_type
  tags         = var.network_tags

  boot_disk {
    source = google_compute_disk.boot_disk.name
  }

  attached_disk {
    source      = google_compute_disk.data_disk[0].name
    device_name = "data"
  }

  network_interface {
    network    = var.network
    subnetwork = var.subnetwork
    network_ip = google_compute_address.private_ip.address

    dynamic "access_config" {
      for_each = var.access_config
      content {
        nat_ip       = access_config.value.nat_ip
        network_tier = access_config.value.network_tier
      }
    }
  }

  labels = { for k, v in var.labels : k => v }

  service_account {
    email  = var.service_account.email != null ? var.service_account.email : null
    scopes = var.service_account.scopes != null ? [for i in var.service_account.scopes : i] : []
  }

  metadata_startup_script = var.metadata_startup_script

  allow_stopping_for_update = var.allow_stopping_for_update
  desired_status            = var.desired_status

  scheduling {
    automatic_restart           = var.scheduling.automatic_restart
    preemptible                 = var.scheduling.preemptible
    provisioning_model          = var.scheduling.provisioning_model
    on_host_maintenance         = var.scheduling.on_host_maintenance
    instance_termination_action = var.scheduling.instance_termination_action
  }
}

# If NOT use attached disk
resource "google_compute_instance" "instance" {
  count        = var.attached_disk_enabled != true ? 1 : 0
  name         = var.instance_name
  machine_type = var.machine_type
  tags         = var.network_tags

  boot_disk {
    source = google_compute_disk.boot_disk.name
  }

  network_interface {
    network    = var.network
    subnetwork = var.subnetwork
    network_ip = google_compute_address.private_ip.address

    dynamic "access_config" {
      for_each = var.access_config
      content {
        nat_ip       = access_config.value.nat_ip
        network_tier = access_config.value.network_tier
      }
    }
  }

  labels = { for k, v in var.labels : k => v }

  service_account {
    email  = var.service_account.email != null ? var.service_account.email : null
    scopes = var.service_account.scopes != null ? [for i in var.service_account.scopes : i] : []
  }

  allow_stopping_for_update = var.allow_stopping_for_update
  desired_status            = var.desired_status

  scheduling {
    automatic_restart           = var.scheduling.automatic_restart
    preemptible                 = var.scheduling.preemptible
    provisioning_model          = var.scheduling.provisioning_model
    on_host_maintenance         = var.scheduling.on_host_maintenance
    instance_termination_action = var.scheduling.instance_termination_action
  }
}


