variable "project" {}
variable "region" {}
variable "zone" {}

variable "instance_name" {
  description = "The name of the VM instance (required)"
}

variable "machine_type" {
  default = "e2-medium"
}

variable "labels" {
  type = map(any)
}

variable "service_account" {
  type = object({
    email  = string
    scopes = list(string)
  })

  default = {
    email  = null
    scopes = []
  }
}

variable "desired_status" {
  default = "RUNNING"
}

variable "allow_stopping_for_update" {
  default = true
}

variable "metadata_startup_script" {}

variable "scheduling" {
  type = object({
    preemptible                 = bool
    automatic_restart           = bool
    provisioning_model          = string
    on_host_maintenance         = string
    instance_termination_action = string
  })

  default = {
    preemptible                 = false
    automatic_restart           = true
    provisioning_model          = "STANDARD"
    on_host_maintenance         = "MIGRATE"
    instance_termination_action = "STOP"
  }
}

###############
### Network ###
###############
variable "network" {
  description = "The VPC network to host the VM instnace in (required)"
}

variable "subnetwork" {
  description = "The VPC subnetwork to host the VM instnace in (required)"
}

variable "network_tags" {
  default = null
}
variable "private_ip" {
  default = null
}

variable "access_config" {
  description = "Access configurations, i.e. IPs via which the VM instance can be accessed via the Internet."
  type = list(object({
    nat_ip       = string
    network_tier = string
  }))
  default = []
}


############
### Disk ###
############
variable "boot_disk_type" {
  default = "pd-standard"
}
variable "boot_disk_size" {
  default = "10"
}
variable "boot_disk_image" {
  default = "debian-cloud/debian-9"
}

variable "attached_disk_enabled" {
  default = false
  type    = bool
}

variable "data_disk_type" {
  default = "pd-standard"
}
variable "data_disk_size" {
  default = "10"
}
