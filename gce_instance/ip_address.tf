# Reserving static internal IP address
resource "google_compute_address" "private_ip" {
  name         = var.instance_name
  address_type = "INTERNAL"
  subnetwork   = var.subnetwork
  address      = var.private_ip
}
