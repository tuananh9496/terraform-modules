### BOOT Disks
resource "google_compute_disk" "boot_disk" {
  name  = "${var.instance_name}-boot"
  type  = var.boot_disk_type
  zone  = var.zone
  image = var.boot_disk_image
  size  = var.boot_disk_size
}

### DATA Disks
resource "google_compute_disk" "data_disk" {
  count = var.attached_disk_enabled ? 1 : 0
  name  = "${var.instance_name}-data"
  type  = var.data_disk_type
  zone  = var.zone
  size  = var.data_disk_size
}